$(function () {
    var socket = io();
    $('form').submit(function(e){
      e.preventDefault(); // prevents page reloading
      let name = $('#nickname').val();
      let msg = $('#message').val();
      socket.emit('chat message', { name, msg });
      append_message({name, msg}, 'self');
      $('#message').val('');
      return false;
    });

    $('#message').focusin(() => {
        let name = $('#nickname').val();
        socket.emit('user typing', name);
    });
    $('#message').focusout(() => {
        socket.emit('user stopped typing');
    });
    socket.on('chat message', ({name, msg}) => {
        append_message({name, msg});
    });
    socket.on('user connect', function(msg) {
        $('#messages').append($('<li class="text-center"><i>' + msg + '</i></li>'));
    })
    socket.on('user typing', function(msg) {
        $('#status').text(msg);
    });
    socket.on('user stopped typing', function() {
        $('#status').text("");
    })
});

function append_message({name, msg}, html_class="") {
    var message_wrapper = $('<div class="' + html_class + '">');
    message_wrapper.append($('<p class="">' + name + '</p>'));
    message_wrapper.append($('<li class="message">').text(msg));
    message_wrapper.append($('<br>'));
    $('#messages').append(message_wrapper);
    autoscroll();
}

function autoscroll() {
    window.scrollTo(0,document.body.scrollHeight);
}