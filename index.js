var express = require('express');
var app = express();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
var xssFilters = require('xss-filters');

const PORT = process.env.PORT || 3000;

app.use(express.static(__dirname));

app.get('/', function(req, res){
    res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket){
    io.emit('user connect', 'A user has connected');
    console.log('a user connected');

    socket.on('chat message', ({name, msg}) => {
        msg = xssFilters.inHTMLData(msg);
        socket.broadcast.emit('chat message', {name, msg});
    });

    socket.on('disconnect', function(){
        io.emit('user connect', 'A user has disconnected');
        console.log('user disconnected');
    });

    socket.on('user typing', function(name) {
        if (!name || name === "" || name === undefined) name = "Anonymous";
        io.emit('user typing', name + ' is typing...');  
    });

    socket.on('user stopped typing', function() {
        io.emit('user stopped typing');
    });
});

http.listen(PORT, function(){
  console.log('listening on: ', PORT);
});